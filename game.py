from bag import Bag
from player import Player
from interaction import *


class Game:
    """
    Класс игры.
    """

    def __init__(self):
        """
        Инициализатор.
        """
        self.bag = Bag()  # мешок
        self.players = self.set_players()  # создать игроков

    def start(self):
        """
        Начало игры.
        """
        if len(self.players) < 2:
            cannot_start_game()
            return
        # пока не останется бочонков в мешке
        while not self.bag.is_empty():
            if len(self.players) >= 2:
                keg_number = self.pull_out_keg()  # достаем бочонок
            for player in self.players:  # для каждого игрока
                # если игрок остался один, то он победитель
                if len(self.players) == 1:
                    display_player_win(player)
                    return
                result = self.move(keg_number, player)  # ход игрока
                if not result:  # если игрок ошибся
                    self.players.remove(player)  # убираем его из игры
                    continue  # к следующему игроку
                if player.card.is_done():  # проверить все ли числа зачеркнуты
                    display_player_win(player)
                    return

    def pull_out_keg(self):
        """
        Достаем бочонок из мешка
        :return: номер бочонка
        """
        keg_number = self.bag.pull_out_keg()  # достаем бочонок
        print(Fore.GREEN + f"Новый бочонок: {keg_number} "
                           f"(осталось {len(self.bag.kegs)})")
        print(Style.RESET_ALL)
        return keg_number

    def move(self, keg_number, player):
        """
        Новый ход игрока.
        :param keg_number: номер текущего бочонка
        :param player: игрок
        :return: результат хода
        """
        # вывести имя игрока на экран
        print(player.name)
        print(player.card)  # вывести карточку игрока на экран
        # проверяем есть ли число в карточке
        exist_in_card = False  # по умолчанию
        if player.card.exist_in_card(keg_number):
            exist_in_card = True  # да, есть
        answer = exist_in_card  # по умолчанию (для компьютера)
        if player.type == 2:  # т.е. человек
            answer = ask()  # спрашиваем игрока что делать
        # ответ может быть утвердительным или отрицательным
        check_answer = self.check_move(answer, exist_in_card)  # проверить ответ
        if player.type == 2:  # т.е. человек
            # вывести надпись на экран (ободряющую или не очень)
            display_player_right() if check_answer \
                else display_player_wrong(player)
        # если игрок ответил верно, что нужно зачеркнуть
        if check_answer and answer:
            player.card.cross_out(keg_number)  # вычеркиваем из карточки
        return check_answer

    def check_move(self, answer, exist_in_card):
        """
        Проверка правильности сделанного хода.
        :param answer: ответ игрока (True или False)
        :param exist_in_card: признак наличия числа в карточке
        """
        if answer and exist_in_card \
                or not answer and not exist_in_card:
            return True
        if answer and not exist_in_card \
                or not answer and exist_in_card:
            return False

    def create_player(self):
        """
        Функция создания игрока.
        :return: объект Player
        """
        # задать тип игрока
        player_type = set_player_type()
        # задать имя игрока
        player_name = set_player_name()
        # создать игрока
        player = Player(player_type, player_name)
        return player

    def set_players(self):
        """
        Функция определения игроков.
        """
        # гарантированно создать хотя бы одного игрока
        players = [self.create_player()]
        # по требованию создать ещё игроков
        while True:
            if not another_player_required():
                break  # выйти если больше игроков не требуется
            players.append(self.create_player())
        return players