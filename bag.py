import random


class Bag:
    """
    Класс мешок с бочонками.
    """

    def __init__(self):
        """
        Инициализатор.
        Автоматически заполняет мешок бочонками
        с номерами от 1 до 90.
        """
        self.kegs = list(range(1, 91))

    def pull_out_keg(self):
        """
        Вытягивает бочонок из мешка.
        Бочонок определяется автоматчиески
        :return: номер бочонка
        """
        # если мешок пуст
        if self.is_empty():
            return 0  # уходим
        # случайный индекс из оставшихся в мешке чисел
        # -1 т.к. должен идти от нуля
        index = random.randint(1, len(self.kegs)) - 1
        keg_number = self.kegs.pop(index)  # вытянуть
        return keg_number

    def is_empty(self):
        """
        Проверка того, что мешок пуст.
        :return: True или False
        """
        if len(self.kegs) == 0:
            return True
        else:
            return False
