from colorama import Fore, Style
import constants


def display_player_wrong(player):
    print(Fore.RED + f"Неверно! Игрок {player.name} проиграл.")
    print(Style.RESET_ALL)


def display_player_right():
    print(Fore.GREEN + "Верно!")
    print(Style.RESET_ALL)


def display_game_over():
    print(Fore.GREEN + "Игра окончена.")
    print(Style.RESET_ALL)


def display_player_win(player):
    print(Fore.GREEN + f"Игрок {player.name} победил!")
    print(Style.RESET_ALL)


def cannot_start_game():
    print(Fore.RED + "Для начала игры требуется минимум 2 игрока!")
    print(Style.RESET_ALL)


def ask():
    """
    Спросить игрока - зачеркиваем цифру?
    :return: True or False
    """
    while True:
        s = input("Зачеркнуть цифру? (y/n) ")
        if s == "y":
            return True
        elif s == "n":
            return False


def set_player_type():
    """
    Создать нового игрока.
    :return: индекс типа игрока
    """
    while True:
        print("Введите тип нового игрока:")
        display_player_types()  # отобразить допустимые типы игроков
        try:
            player_type = int(input())
            if player_type > len(constants.PLAYER_TYPES) \
                    or player_type <= 0:  # ограничение
                print(Fore.GREEN + f"Требуется ввести число от 1 "
                                   f"до {len(constants.PLAYER_TYPES)}!")
            else:
                break
        except ValueError:
            print(Fore.GREEN + f"Требуется ввести число от 1 "
                               f"до {len(constants.PLAYER_TYPES)}!")
        finally:
            print(Style.RESET_ALL)
    print(Fore.GREEN + "Выбран тип игрока: ",
          constants.PLAYER_TYPES[player_type])
    print(Style.RESET_ALL)
    return player_type


def display_player_types():
    """
    Отобразить возможные типы игроков
    :return:
    """
    for t in constants.PLAYER_TYPES:
        print(f"\t{t}: {constants.PLAYER_TYPES[t]}")


def set_player_name():
    """
    Задать имя игрока
    :return: заданное имя игрока
    """
    player_name = input("Введите имя нового игрока: ")  # задать текст запроса
    print(Fore.GREEN + "Вы ввели: ", player_name)
    print(Style.RESET_ALL)
    return player_name


def another_player_required():
    """
    Функция проверки требования создания
    ещё одного игрока.
    :return: True или False
    """
    while True:
        s = input("Требуется ли создание ещё одного игрока (y/n)? ")
        if s == "y" \
                or s == "yes":
            required = True
            break
        elif s == "n" \
                or s == "no":
            required = False
            break
    print(Fore.GREEN + "Ваш ответ: ", "да" if required else "нет")
    print(Style.RESET_ALL)
    return required
