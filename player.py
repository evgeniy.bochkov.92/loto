import constants
from card import Card


class Player:
    """
    Класс игрока.
    """

    def __init__(self, type, name):
        """
        Зачеркнуть число в карточке.
        :param type: тип игрока (компьютер или человек)
        :param name: имя игрока
        """
        if not type in range(1, len(constants.PLAYER_TYPES) + 1):
            raise ValueError("invalid argument!")
        self.type = type
        self.name = name
        self.card = Card()  # карточка генерируется автоматически
