from card import Card


def test_card_has_15_unique_numbers():
    """
    Проверить что карточка после создания
    содержит 15 цифр. И они не повторяются.
    Числа от 1 до 90.
    """
    card = Card()
    assert len(set(card.cells)) == 15
    assert min(card.cells) >= 1
    assert max(card.cells) <= 90


def test_cross_out_cell():
    """
    Проверить что зачеркивается число в карточке.
    """
    card = Card()
    card.cells = [1, 2, 3, 4, 5, 6, 7, 8, 9,
                  10, 11, 12, 13, 14, 15]
    crossed_out_cell = 13
    card.lines = card.create_lines()
    card.cross_out(crossed_out_cell)
    assert crossed_out_cell not in card.cells
