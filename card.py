import random


class Card:
    """
    Класс карточка игрока.
    TODO избавиться от магических чисел
    TODO подумать как можно улучшить
    """

    def __init__(self):
        # 3 строки, в каждой 5 цифр = 15 цифр
        # не должны повторяться!
        # также 4 ячейки в строке свободных
        # генерируем 15 случайных чисел
        self.cells = random.sample(range(1, 90 + 1), 15)
        # разбиваем на строки по 5 чисел в каждой
        self.lines = self.create_lines()

    def create_lines(self):
        """
        Создать табличный вид карточки.
        Карточка состоит из трех строк.
        В каждой строке 5 ячеек с числами и
        4 пустых ячейки.
        :return: список из трех строк
        """
        lines = list(range(3))  # 3 строки
        for i in range(0, len(lines)):
            lines[i] = self.cells[i * 5:i * 5 + 5]
            lines[i].sort()  # сортируем
            self.add_spaces(lines[i])
        return lines

    def __str__(self):
        """
        Второй магический метод.
        Выводит карточку на экран.
        """
        out = "----------------------------\n"
        for i in self.lines:
            row = ""
            for k in i:
                row += "%3s" % k
            out += row + "\n"
        out += "----------------------------\n"
        return out

    def add_spaces(self, s):
        """
        Генерируем пустые ячейки для красоты
        случайным образом.
        """
        # случайно определяем индексы пустых ячеек
        spaces = random.sample(range(0, 9), 4)
        spaces.sort()  # по возрастанию
        for i in range(0, 4):
            s.insert(spaces[i], "")

    def cross_out(self, num):
        """
        Зачеркнуть число в карточке.
        :param num: номер бочонка
        """
        # ищем число в карточке
        i, index = self.get_index(num)
        self.lines[i][index] = "-"  # зачеркиваем
        # также удаляем из нашего "реестра" чисел
        if num in self.cells:
            self.cells.remove(num)

    def get_index(self, num):
        """
        Получить координаты числа в карточке.
        :param num: число
        :return: номер строки и номер столбца
        """
        # обегаем строчки карточки
        for i in range(0, len(self.lines)):
            # если нашли число в строке
            if num in self.lines[i]:
                # забираем индекс
                index = self.lines[i].index(num)
                # вернуть номер строки и номер столбца
                return i, index

    def exist_in_card(self, num):
        """
        Проверка существования числа в карточке.
        :param num: номер бочонка
        :return: True или False
        """
        return num in self.cells

    def is_done(self):
        """
        Проверка того, что все числа карточки зачеркнуты.
        :return: True или False
        """
        if len(self.cells) == 0:
            return True
        else:
            return False
