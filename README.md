# Loto

Игра ЛОТО для ДЗ №2

Реализовано:
+ создание произвольного количества игроков
+ указание типа игрока при создании - человек или компьютер
+ Dockerfile для запуска в контейнере


Реализованы тесты для ДЗ №3

+ проверить что в мешке 90 бочонков
+ проверить корректность создания игрока
+ проверить что в карточке 15 уникальных чисел
+ проверить что вытягивается бочонок из мешка
+ проверить что зачеркивается число в карточке

Тесты проходят успешно:

C:\Users\bochkovea\PycharmProjects\loto>pytest -v
======================================================== test session starts =========================================================
platform win32 -- Python 3.9.4, pytest-6.2.4, py-1.10.0, pluggy-0.13.1 -- c:\users\bochkovea\appdata\local\programs\python\python39\python.exe
cachedir: .pytest_cache
rootdir: C:\Users\bochkovea\PycharmProjects\loto
collected 5 items

test_bag.py::test_bag_has_90_kegs PASSED                                                                                        [ 20%]
test_bag.py::test_pull_out_keg PASSED                                                                                           [ 40%]
test_card.py::test_card_has_15_unique_numbers PASSED                                                                            [ 60%]
test_card.py::test_cross_out_cell PASSED                                                                                        [ 80%]
test_player.py::test_player_creation PASSED                                                                                     [100%]

========================================================= 5 passed in 0.09s ==========================================================
