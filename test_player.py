from player import Player


def test_player_creation():
    """
    Проверить что игрок создается корректно.
    """
    player = Player(1, "Charles Perrault")
    assert player.type == 1
    assert player.name == "Charles Perrault"
