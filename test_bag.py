from bag import Bag


def test_bag_has_90_kegs():
    """
    Проверить что в мешке при инициализации
    ровно 90 бочонков. Числа от 1 до 90.
    """
    bag = Bag()
    assert len(bag.kegs) == 90
    assert min(bag.kegs) >= 1
    assert max(bag.kegs) <= 90


def test_pull_out_keg():
    """
    Проверить что вытаскивается бочонок
    из мешка.
    """
    bag = Bag()
    bag.kegs = [1]
    keg = bag.pull_out_keg()
    assert keg == 1
